/// <reference path="jquery-2.1.1.js" />
/// <reference path="underscore.js" />
/// <reference path="backbone.js" />

var ComponentsPanel = {
    Models: {},
    Collections: {},
    Views: {},
    Templates: {}
};



ComponentsPanel.Models.Component = Backbone.Model.extend({});

ComponentsPanel.Collections.Components = Backbone.Collection.extend({
    model: ComponentsPanel.Models.Component,
    url: "../components.json",
    initialize: function(){
        console.log("Components Initialize");
    }
});

ComponentsPanel.Views.Components = Backbone.View.extend({
    
    initialize: function(){
        this.collection.bind("reset", this.render, this);
        this.collection.bind("add", this.addOne, this);
    },
    
    render: function(){
        console.log("render");
        console.log(this.collection.length);
        this.addAll();
    },
    
    addAll: function(){
        console.log("addAll");
        this.collection.each(this.addOne);
    },
    
    addOne: function(model){
        console.log("addOne");
        view = new ComponentsPanel.Views.Component({ model:             model });
        view.render();
    }
})

ComponentsPanel.Templates.component = _.template($("#tmplt-component").html())
ComponentsPanel.Views.Component = Backbone.View.extend({
    
    template: ComponentsPanel.Templates.component,
    
    initialize: function (){
        this.model.bind('destroy', this.destroyItem, this);
        this.model.bind('remove', this.removeItem, this);
    },
    
    render: function (){            
        $("#ComponentsList").append(this.template(this.model.toJSON())) ;

    },
    
    removeItem: function(model){
        console.log("Remove - " + model.get("Name"))
        this.remove();
    }
})

ComponentsPanel.Router = Backbone.Router.extend({
    routes: {
        "": "defaultRoute"
    },
    
    defaultRoute: function() {
        console.log("defaultRoute");
        
        ComponentsPanel.components = new ComponentsPanel.Collections.Components()
        new ComponentsPanel.Views.Components({collection: ComponentsPanel.components});
        ComponentsPanel.components.fetch();
        console.log(ComponentsPanel.components.length)
    }
    
})

var appRouter = new ComponentsPanel.Router();

Backbone.history.start();

$("#addComponentButton").click(null, function () {
    console.log("ADD");

    var newComponent = new ComponentsPanel.Models.Component(
        {
        "pic": "../images/steam.gif",
        "tittle": "Test",
        "author_pic": "../images/avatar-06.svg"
        }
    );

    ComponentsPanel.components.add(newComponent);
    console.log(ComponentsPanel.components.length)
})

