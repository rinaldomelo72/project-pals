var App = angular.module('App', []);

App.controller('listProjects', function($scope, $http) {
    
    $scope.remove = function ($id) {$scope.projects.splice($id,1)};
    
    $http.get('data/projects.json').then(function(res){
        $scope.projects = res.data});
    
});