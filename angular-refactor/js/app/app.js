var app = angular.module('ProjectPalsWorkspace', [
	'ui.router']);

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('listComponents', {
            
            url: "/components",
            views: {
                'main': {
                    templateUrl: 'templates/components.html'
                },
                'search':{
                    templateUrl: 'templates/search-bar.html',
                    controller: 'ComponentsListController'
                }
             
            }
        })
        .state('listProjects', {
            url: "/",
            views: {
                'main': {
                    templateUrl: 'templates/projects.html'
                },
                'search':{
                    templateUrl: 'templates/search-bar.html',
                    controller: 'ListProjectsController'
                }
            }
        });

    $urlRouterProvider.otherwise('/');
});

app.controller('ComponentsListController', function ($scope, $rootScope ,$state, $http, ComponentService, SearchService) {
    $scope.mode = "Components";
    $scope.componentService = ComponentService;
    $scope.searchService = SearchService;

    $http.get('data/components.json').then(function (res) {
        $scope.components = res.data;
        $scope.components = $scope.components[$rootScope.selectedProject.id] 
    });

});

app.controller('ComponentsDetailController', function ($scope, $state, ComponentService) {
    $scope.mode = "Components";
    $scope.componentService = ComponentService;

});


app.controller('ListProjectsController', function($scope, $rootScope ,$http, SearchService) {
    $scope.mode = "Projects";
    $scope.searchService = SearchService;
    $rootScope.selectedProject = null;
    $rootScope.isSelected = function (){return $rootScope.selectedProject != null};
    
    $scope.remove = function ($id) {$scope.projects.splice($id,1)};
    $scope.selectProject = function ($project) {
        $rootScope.selectedProject = $project
    };
    
    $http.get('data/projects.json').then(function(res){
    $scope.projects = res.data});
    
});

app.service('ComponentService', function () {
    return {
        'selectedComponent': null
    };
});


app.service('SearchService', function () {
    return {
        'search': ''
    };
});